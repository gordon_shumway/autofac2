using System;
using System.Collections.Generic;
using System.Linq;

using Autofac.Features.Indexed;
using Autofac.Features.Metadata;

using Autofac2.ConsoleApp.Commands;

namespace Autofac2.ConsoleApp
{
    public class CommandParser : ICommandParser
    {
        private readonly IEnumerable<Meta<Func<ICommand>>> _commandLookup;

        private readonly IInputWrapper _input;
        
        public CommandParser(ILogger logger, IInputWrapper inputWrapper, IEnumerable<Meta<Func<ICommand>>> commandLookup)
        {
            _commandLookup = commandLookup;
            _input = inputWrapper;

            logger.Log("Command Parser created.");
        }

        public ICommand Parse()
        {
            var commandName = _input.ReadCommand();

            CommandType commandType;
            switch (commandName)
            {
                case "turn-on":
                    commandType = CommandType.TurnOn;
                    break;
                case "turn-off":
                    commandType = CommandType.TurnOff;
                    break;
                case "exit":
                    commandType = CommandType.Exit;
                    break;
                default:
                    commandType = CommandType.Empty;
                    break;
            }

            return _commandLookup.Single(m => (CommandType)m.Metadata["CommandType"] == commandType).Value();
        }
    }
}