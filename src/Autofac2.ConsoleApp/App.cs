using System;
using System.Collections.Generic;
using System.Linq;

using Autofac2.ConsoleApp.Commands;

namespace Autofac2.ConsoleApp
{
    public class App : IApp
    {
        private readonly IEnumerable<ILogger> _loggers;

        private readonly Tv.Factory _tvFactory;

        public ICommandParser CommandParser { get; set; }
       
        public App(IEnumerable<ILogger> loggers, Tv.Factory tvFactory)
        {
            _loggers = loggers;
            _tvFactory = tvFactory;

            _loggers.ToList().ForEach(l => l.Log("App created."));
        }

        public void Run()
        {
            _loggers.ToList().ForEach(l => l.Log("Application is running..."));

            var tv = _tvFactory("Samsung", "LED Smart TV");

            ICommand command;
            do
            {
                _loggers.ToList().ForEach(l => l.Info($"{tv.Info}"));
                command = CommandParser.Parse();
            }
            while (command.Execute(tv));
        }
    }
}