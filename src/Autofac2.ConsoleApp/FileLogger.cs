﻿using System;
using System.IO;

namespace Autofac2.ConsoleApp
{
    public class FileLogger : ILogger
    {
        public void Log(string message)
        {
            Write(message);
        }

        public void Info(string message)
        {
            Write(message, "INFO");
        }

        public void Warn(string message)
        {
            Write(message, "WARN");
        }

        private void Write(string message, string prefix = "")
        {
            using (var writer = new StreamWriter("log.txt", true))
            {
                if (!string.IsNullOrWhiteSpace(prefix))
                {
                    writer.Write($"{prefix}: ");
                }

                writer.WriteLine($"{DateTime.Now}: {message}");
            }
        }
    }
}
