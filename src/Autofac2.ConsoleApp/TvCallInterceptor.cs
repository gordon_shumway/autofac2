﻿using Castle.DynamicProxy;

namespace Autofac2.ConsoleApp
{
    public class TvCallInterceptor : IInterceptor
    {
        private readonly ILogger _logger;

        public TvCallInterceptor(ILogger logger)
        {
            _logger = logger;
        }

        public void Intercept(IInvocation invocation)
        {
            _logger.Info($"{invocation.Method.Name} started...");
            invocation.Proceed();
            _logger.Info($"{invocation.Method.Name} ended...");
        }
    }
}
