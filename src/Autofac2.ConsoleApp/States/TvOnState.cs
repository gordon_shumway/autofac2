namespace Autofac2.ConsoleApp.States
{
    public class TvOnState : ITvState
    {
        public TvOnState(ILogger logger)
        {
            logger.Log("TvOnState created.");
        }

        public TvState State { get; } = TvState.On;

        public string Info { get; } = "Tv is ON.";
    }
}