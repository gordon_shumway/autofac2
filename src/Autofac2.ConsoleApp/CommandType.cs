namespace Autofac2.ConsoleApp
{
    public enum CommandType
    {
        TurnOn, TurnOff, Exit, Empty
    }
}