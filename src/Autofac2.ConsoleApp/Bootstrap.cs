﻿using System.Reflection;

using Autofac;
using Autofac.Extras.DynamicProxy;
using Autofac2.ConsoleApp.States;

namespace Autofac2.ConsoleApp
{
    public static class Bootstrap
    {
        public static IApp Configure()
        {
            var containerBuilder = new ContainerBuilder();
            
            //containerBuilder.RegisterType<App>().As<IApp>().PropertiesAutowired();
            containerBuilder
                .RegisterType<App>()
                .As<IApp>()
                .OnActivated(args => args.Instance.CommandParser = args.Context.Resolve<ICommandParser>());

            containerBuilder.RegisterType<ConsoleLogger>().As<ILogger>().SingleInstance();
            containerBuilder.RegisterType<FileLogger>().As<ILogger>().PreserveExistingDefaults().SingleInstance();
            containerBuilder.RegisterType<CommandParser>().As<ICommandParser>();
            containerBuilder.RegisterType<InputWrapper>().As<IInputWrapper>();

            containerBuilder.RegisterType<TvOnState>().Keyed<ITvState>(TvState.On);
            containerBuilder.RegisterType<TvOffState>().Keyed<ITvState>(TvState.Off);

            containerBuilder.Register(c => new TvCallInterceptor(c.Resolve<ILogger>()));
            containerBuilder.RegisterType<Tv>().As<ITv>().EnableInterfaceInterceptors();//.InterceptedBy(typeof(TvCallInterceptor));

            var commandsAssembly = Assembly.LoadFrom("Autofac2.Commands.dll");
            containerBuilder.RegisterAssemblyModules(commandsAssembly);

            var container = containerBuilder.Build();

            return container.Resolve<IApp>();
        }
    }
}