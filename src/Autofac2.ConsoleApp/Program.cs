﻿namespace Autofac2.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var app = Bootstrap.Configure();
            app.Run();
        }
    }
}
