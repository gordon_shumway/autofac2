using Autofac.Extras.DynamicProxy;
using Autofac.Features.Indexed;

using Autofac2.ConsoleApp.States;

namespace Autofac2.ConsoleApp
{
    [Intercept(typeof(TvCallInterceptor))]
    public class Tv : ITv
    {
        public delegate ITv Factory(string name, string type);

        private readonly ILogger _logger;

        private readonly IIndex<TvState, ITvState> _tvStateLookup;

        public ITvState State { get; private set; }

        public string Info => State.Info;

        public string Name { get; }

        public string Type { get; }

        public Tv(string name, string type, ILogger logger, IIndex<TvState, ITvState> tvStateLookup)
        {
            Name = name;
            State = tvStateLookup[TvState.Off];
            Type = type;

            _logger = logger;
            _tvStateLookup = tvStateLookup;

            _logger.Log($"Tv {Name} {Type} created.");
        }

        public void TurnOn()
        {
            if (State.State == TvState.On)
            {
                _logger.Warn("Tv is already ON!");
                return;
            }

            State = _tvStateLookup[TvState.On];
        }

        public void TurnOff()
        {
            if (State.State == TvState.Off)
            {
                _logger.Warn("Tv is already OFF!");
                return;
            }

            State = _tvStateLookup[TvState.Off];
        }
    }
}