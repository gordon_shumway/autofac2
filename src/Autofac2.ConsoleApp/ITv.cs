using Autofac2.ConsoleApp.States;

namespace Autofac2.ConsoleApp
{
    public interface ITv
    {
        ITvState State { get; }

        string Info { get; }

        string Name { get; }

        string Type { get; }

        void TurnOn();

        void TurnOff();
    }
}