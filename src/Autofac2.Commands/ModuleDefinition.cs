﻿using Autofac;

using Autofac2.ConsoleApp;
using Autofac2.ConsoleApp.Commands;

namespace Autofac2.Commands
{
    public class ModuleDefinition : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<TurnOnCommand>().Named<ICommand>("command").WithMetadata("CommandType", CommandType.TurnOn);
            builder.RegisterType<TurnOffCommand>().Named<ICommand>("command").WithMetadata("CommandType", CommandType.TurnOff);
            builder.RegisterType<ExitCommand>().Named<ICommand>("command").WithMetadata("CommandType", CommandType.Exit);
            builder.RegisterType<EmptyCommand>().Named<ICommand>("command").WithMetadata("CommandType", CommandType.Empty);

            builder.RegisterDecorator<ICommand>((c, inner) => new CommandDecorator(inner, c.Resolve<ILogger>()), fromKey: "command");
        }
    }
}
