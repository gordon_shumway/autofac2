﻿using Autofac2.ConsoleApp;
using Autofac2.ConsoleApp.Commands;

namespace Autofac2.Commands
{
    public class CommandDecorator : ICommand
    {
        private readonly ICommand _inner;

        private readonly ILogger _logger;

        public CommandDecorator(ICommand inner, ILogger logger)
        {
            _inner = inner;
            _logger = logger;
        }

        public bool Execute(ITv tv)
        {
            var result = _inner.Execute(tv);
            _logger.Log($"{_inner.GetType().Name} executed...");

            return result;
        }
    }
}
