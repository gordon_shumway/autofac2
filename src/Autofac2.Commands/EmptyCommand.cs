using Autofac2.ConsoleApp;
using Autofac2.ConsoleApp.Commands;

namespace Autofac2.Commands
{
    public class EmptyCommand : ICommand
    {
        private readonly ILogger _logger;

        public EmptyCommand(ILogger logger)
        {
            _logger = logger;
            _logger.Log("Empty Command created.");
        }

        public bool Execute(ITv tv)
        {
            _logger.Warn("Unknown command!");

            return true;
        }
    }
}