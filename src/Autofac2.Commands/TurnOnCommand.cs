using Autofac2.ConsoleApp;
using Autofac2.ConsoleApp.Commands;

namespace Autofac2.Commands
{
    public class TurnOnCommand : ICommand
    {
        private readonly ILogger _logger;

        public TurnOnCommand(ILogger logger)
        {
            _logger = logger;
            _logger.Log("Turn On Command created.");
        }

        public bool Execute(ITv tv)
        {
            tv.TurnOn();
            return true;
        }
    }
}
