using Autofac2.ConsoleApp;
using Autofac2.ConsoleApp.Commands;

namespace Autofac2.Commands
{
    public class TurnOffCommand : ICommand
    {
        private readonly ILogger _logger;

        public TurnOffCommand(ILogger logger)
        {
            _logger = logger;
            _logger.Log("Turn Off Command created.");
        }

        public bool Execute(ITv tv)
        {
            tv.TurnOff();
            return true;
        }
    }
}